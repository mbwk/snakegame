#pragma once

#include <QBasicTimer>
#include <QFrame>
#include <QPointer>

enum class SnakeBlock
{
    Nothing,
    Wall,
    SnakeHead,
    SnakePart,
    Food
};

enum class Direction
{
    Up,
    Down,
    Left,
    Right
};

class SnakeCanvas : public QFrame
{
    Q_OBJECT

public:
    SnakeCanvas(QWidget *parent = nullptr);

public slots:
    void start();
    void pause();

signals:
    void scoreChanged(int score);
    void levelChanged(int level);

protected:
    void paintEvent(QPaintEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void timerEvent(QTimerEvent *event) override;

private:
    const static int FIELD_HEIGHT = 30;
    const static int FIELD_WIDTH = 40;
    int level;
    int score;

    void checkCollision();
    Direction checkTurnSelf(Direction newDirection);
    void drawSquare(QPainter &painter, int x, int y, SnakeBlock blk);
    SnakeBlock &blkAt(int x, int y) { return field[(y * FIELD_WIDTH) + x]; }
    int timeoutTime() { return 200 / (1 + level); }
    int squareWidth() { return contentsRect().width() / FIELD_WIDTH; }
    int squareHeight() { return contentsRect().height() / FIELD_HEIGHT; }
    void clearField();
    void moveSnake();
    void growSnake();

    QBasicTimer timer;
    bool isStarted;
    bool isPaused;
    SnakeBlock field[FIELD_HEIGHT * FIELD_WIDTH];
    int headX;
    int headY;
    std::list<Direction> snakeBody;
    Direction currentDirection;
};

