#include "snakecanvas.hpp"

#include <QtWidgets>

std::map<Direction, Direction> OPPOSITE_DIRECTION = {
    {Direction::Up, Direction::Down},
    {Direction::Down, Direction::Up},
    {Direction::Left, Direction::Right},
    {Direction::Right, Direction::Left}
};

SnakeCanvas::SnakeCanvas(QWidget *parent)
    : QFrame(parent), level(1)
{
    setFrameStyle(QFrame::Panel | QFrame::Sunken);
    setFocusPolicy(Qt::StrongFocus);

    isStarted = false;
    isPaused = false;
    currentDirection = Direction::Left;
    headX = FIELD_WIDTH / 2;
    headY = FIELD_HEIGHT / 2;

    clearField();
}

void
SnakeCanvas::start()
{
    if (isPaused) {
        return;
    }

    isStarted = true;
    level = 1;
    currentDirection = Direction::Left;
    score = 0;
    level = 0;
    clearField();

    emit scoreChanged(score);
    emit levelChanged(level);

    headX = FIELD_WIDTH / 2;
    headY = FIELD_HEIGHT / 2;

    snakeBody.clear();
    for (int i = 0; i < 3; ++i) {
        growSnake();
    }

    timer.start(timeoutTime(), this);
}

void
SnakeCanvas::pause()
{
    if (!isStarted) {
        return;
    }

    isPaused = !isPaused;
    if (isPaused) {
        timer.stop();
    } else {
        timer.start(timeoutTime(), this);
    }
    update();
}

void
SnakeCanvas::drawSquare(QPainter &painter, int x, int y, SnakeBlock blk)
{
    static std::map<SnakeBlock, QRgb> colorTable = {
        {SnakeBlock::Nothing, 0xFFFFFF},
        {SnakeBlock::Wall, 0x000000},
        {SnakeBlock::SnakeHead, 0x44FF00},
        {SnakeBlock::SnakePart, 0x00FF00},
        {SnakeBlock::Food, 0xFF0000}
    };

    QColor color = colorTable[blk];
    painter.fillRect(x + 1, y + 1, squareWidth() - 2, squareHeight() - 2,
                     color);

    painter.setPen(color.lighter());
    painter.drawLine(x, y + squareHeight() - 1, x, y);
    painter.drawLine(x, y, x + squareWidth() - 1, y);

    painter.setPen(color.darker());
    painter.drawLine(x + 1, y + squareHeight() - 1,
                     x + squareWidth() - 1, y + squareHeight() - 1);
    painter.drawLine(x + squareWidth() - 1, y + squareHeight() - 1,
                     x + squareWidth() - 1, y + 1);
}

void
SnakeCanvas::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    QPainter painter(this);
    QRect rect = contentsRect();

    if (isPaused) {
        painter.drawText(rect, Qt::AlignCenter, tr("Pause"));
        return;
    }

    int fieldTop = rect.bottom() - FIELD_HEIGHT * squareHeight();

    for (int i = 0; i < FIELD_HEIGHT; ++i) {
        for (int j = 0; j < FIELD_WIDTH; ++j) {
            SnakeBlock blk = blkAt(j, FIELD_HEIGHT - i - 1);
            if (blk != SnakeBlock::Nothing) {
                drawSquare(painter, rect.left() + j * squareWidth(),
                           fieldTop + i * squareHeight(), blk);
            }
        }
    }

    drawSquare(painter,
               rect.left() + headX * squareWidth(),
               fieldTop + headY * squareHeight(),
               SnakeBlock::SnakeHead);
    int offsetX = 0, offsetY = 0;
    for (Direction d : snakeBody) {
        switch (d) {
        case Direction::Left:
            --offsetX;
            break;
        case Direction::Right:
            ++offsetX;
            break;
        case Direction::Up:
            --offsetY;
            break;
        case Direction::Down:
            ++offsetY;
            break;
        }
        drawSquare(painter,
                   rect.left() + ((headX + offsetX) * squareWidth()),
                   fieldTop + ((headY + offsetY) * squareHeight()),
                   SnakeBlock::SnakePart);
    }
}

Direction
SnakeCanvas::checkTurnSelf(Direction newDirection)
{
    return newDirection == snakeBody.front() ? currentDirection : newDirection;
}

void
SnakeCanvas::keyPressEvent(QKeyEvent *event)
{
    auto key = event->key();

    switch (key) {
    case Qt::Key_Left:
        currentDirection = checkTurnSelf(Direction::Left);
        break;
    case Qt::Key_Right:
        currentDirection = checkTurnSelf(Direction::Right);
        break;
    case Qt::Key_Up:
        currentDirection = checkTurnSelf(Direction::Up);
        break;
    case Qt::Key_Down:
        currentDirection = checkTurnSelf(Direction::Down);
        break;
    case Qt::Key_S:
        start();
        break;
    case Qt::Key_P:
        pause();
        break;
    }
}

void
SnakeCanvas::growSnake()
{
    snakeBody.push_back(!snakeBody.empty() ? snakeBody.back() : OPPOSITE_DIRECTION[currentDirection]);
}

void
SnakeCanvas::checkCollision()
{
    // check collision with bodyparts
    /* TODO collision
    static int SKIP_PARTS = 3;
    for (int i = 0; i < SKIP_PARTS) {

    }
    */
    // gameOver();
}

void
SnakeCanvas::moveSnake()
{
    switch (currentDirection) {
    case Direction::Up:
        --headY;
        break;
    case Direction::Down:
        ++headY;
        break;
    case Direction::Left:
        --headX;
        break;
    case Direction::Right:
        ++headX;
        break;
    }

    snakeBody.push_front(OPPOSITE_DIRECTION[currentDirection]);
    snakeBody.pop_back();

    checkCollision();

    update();
}

void
SnakeCanvas::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer.timerId()) {
        moveSnake();
    } else {
        QFrame::timerEvent(event);
    }
}

void
SnakeCanvas::clearField()
{
    for (int i = 0; i < FIELD_HEIGHT * FIELD_WIDTH; ++i) {
        field[i] = SnakeBlock::Nothing;
    }
}

