#include <QtWidgets>

#include "snakecanvas.hpp"
#include "snakewindow.hpp"

SnakeWindow::SnakeWindow(QWidget *parent)
    : QWidget(parent)
{
    canvas = new SnakeCanvas;

    auto *layout = new QHBoxLayout;
    layout->addWidget(canvas);
    setLayout(layout);
    setWindowTitle(tr("Snake"));
    resize(40 * 30, 30 * 30);
}

