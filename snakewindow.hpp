#pragma once

#include <QWidget>

class SnakeCanvas;

class SnakeWindow : public QWidget
{
    Q_OBJECT

public:
    SnakeWindow(QWidget *parent = nullptr);

private:
    SnakeCanvas *canvas;
};

